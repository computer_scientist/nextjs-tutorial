import Link from "next/link";
import Head from "next/head";
import { BlogList } from "../components/blog-list";
import styles from "../styles/main.module.scss";
import { getSortedPostsData } from "../lib/posts";
import { InferGetServerSidePropsType, GetServerSideProps } from "next";

/**
 * Represents a functional component for rendering a blog page.
 *
 * @param {Object} props - The component props.
 * @param {Array} props.allPostsData - The array of post objects containing blog data.
 * @returns {JSX.Element} The JSX element representing the rendered blog page.
 */
const Blog = ({
    allPostsData,
}): InferGetServerSidePropsType<typeof getServerSideProps> => {
    return (
        <>
            <Head>
                <title>Blog</title>
            </Head>
            <div className={styles.postContainer}>
                <h1 className={styles.postHeader}>Blog</h1>
                <h2
                    className={`${styles.postContent} ${styles.navigationLink}`}>
                    <Link href={"/"}>Back to home</Link>
                </h2>
                <BlogList allPostsData={allPostsData} />
            </div>
        </>
    );
};
export default Blog;

/**
 * Fetches and returns data to be used as props for server-side rendering.
 *
 * @returns {Promise<Object>} The props object containing the fetched data.
 */
export const getServerSideProps: GetServerSideProps = async () => {
    const allPostsData = getSortedPostsData();

    return {
        props: {
            allPostsData,
        },
    };
};
