import Link from "next/link";
import Head from "next/head";
import { Main } from "../components/main";
import { Footer } from "../components/footer";
import styles from "../styles/main.module.scss";
import React from "react";

/**
 * Home component.
 *
 * @returns {React.JSX.Element} The rendered Home component.
 */
const Home: React.FC = (): React.JSX.Element => {
    return (
        <div className={styles.container}>
            <Head>
                <title>Create Next App</title>
                <link rel={"icon"} href={"/favicon.ico"} />
            </Head>
            <Main />
            <Footer />
        </div>
    );
};
export default Home;
