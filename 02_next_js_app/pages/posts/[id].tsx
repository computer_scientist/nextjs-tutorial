import Head from "next/head";
import styles from "../../styles/main.module.scss";
import Link from "next/link";
import Date from "../../components/date";
import { getPostData } from "../../lib/posts";
import { InferGetServerSidePropsType, GetServerSideProps } from "next";

/**
 * Represents a blog post.
 *
 * @param postData - The data of the blog post.
 * @param postData.title - The title of the blog post.
 * @param postData.date - The date of the blog post.
 * @param postData.contentHtml - The content of the blog post in HTML format.
 * @returns {InferGetServerSidePropsType<typeof getServerSideProps>} - The rendered blog post component.
 */
const Post = ({
    postData,
}: {
    postData: {
        title: string;
        date: string;
        contentHtml: string;
    };
}): InferGetServerSidePropsType<typeof getServerSideProps> => {
    return (
        <>
            <Head>
                <title>{postData.title}</title>
            </Head>
            <article className={styles.blogPostContainer}>
                <h1 className={styles.postHeaderSm}>{postData.title}</h1>
                <h2
                    className={`${styles.postContent} ${styles.navigationLink}`}>
                    <Link href={"/blog"}>Back to Blog</Link>
                </h2>
                <div className={`${styles.lightText} ${styles.blogContent}`}>
                    <Date dateString={postData.date} />
                </div>
                <div
                    className={styles.blogContent}
                    dangerouslySetInnerHTML={{ __html: postData.contentHtml }}
                />
            </article>
        </>
    );
};
export default Post;

/**
 * Retrieves the server-side props for a page.
 *
 * @async
 * @param {object} context - The context object containing parameters.
 * @param {object} context.params - The parameters sent to the page.
 * @param {string} context.params.id - The ID parameter for the page.
 * @returns {Promise<object>} A promise that resolves to an object with the server-side props.
 */
export const getServerSideProps: GetServerSideProps = async ({ params }) => {
    const postData = await getPostData(params?.id as string);

    return {
        props: {
            postData,
        },
    };
};
