import "../styles/globals.scss";
import { AppProps } from "next/app";
import React from "react";

/**
 * Renders the App component with given pageProps.
 *
 * @param {Object} props - The props object that contains Component and pageProps.
 * @param {React.ComponentType} props.Component - The component to be rendered.
 * @param {Object} props.pageProps - The additional props to be passed to the component.
 * @returns {React.JSX.Element} The rendered component.
 */
const App = ({ Component, pageProps }: AppProps): React.JSX.Element => {
    return <Component {...pageProps} />;
};
export default App;
