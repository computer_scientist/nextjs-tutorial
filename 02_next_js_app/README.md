# 02 Next.js Official App

This is a continuation of the official Next.js App. Instead of continuing with
one giant app and removing essential material that was introduced, this project
basically forked each iteration.

In this iteration, a major deviation was made in relation to the official instruction
of the material. All JavaScript files was re-written in TypeScript. Therefore, the .js
files was replaced with .tsx (usually .ts) files. Since Next.js handles TypeScript automatically
with required dependencies, this was an attractive approach which will be used throughout the
remainder of the iterations.

### Adding TypeScript to Project.

To use TypeScript with Next.js the following installation needs to be made from npm.

```bash
npm install --save-dev typescript @types/react @types/node
```

With the installation of next@latest, tsconfig.json and next-env.d.ts files are created
automatically. The following is how the package.json file should look at this point with
scripts, dependencies, and dev-dependencies.

```json
{
  "main": "index.js",
  "scripts": {
    "dev": "next dev",
    "build": "next build",
    "start": "next start",
    "lint": "next lint"
  },
  "dependencies": {
    "next": "^13.4.12",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "sass": "^1.64.2"
  },
  "devDependencies": {
    "@types/node": "20.4.7",
    "@types/react": "18.2.18",
    "typescript": "5.1.6"
  }
}
```

### First Post Material

For the First Post tutorial, this project deviated from the official instruction
a little. First, instead of defacing the title of the main page, a card was created
instead to navigate to the first post page. This seamed to make a bit more sense in
preserving the look and flow of the main page.

For the First Post page, it included styling to enhance the feel of the page, and to be
used as a reference on how to center content using css grid. These styles were added to
the _posts.scss file in the styles' directory.

### Layouts section of official tutorial

The layouts section of the tutorial was not incorporated since it's not my style of creating
components.

### Posts Gray Matter

gray-matter is a package that helps parse the metadata in each markdown file. It can be installed
to the project with the following command:

```zsh
npm install gray-matter
```


