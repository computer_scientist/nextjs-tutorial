import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { remark } from "remark";
import html from "remark-html";

/**
 * Represents the data for a post.
 * @interface
 */
export interface PostData {
    id: string;
    date: string;
    title: string;
}

const postsDirectory: string = path.join(process.cwd(), "posts");

/**
 * Retrieves and returns an array of posts data sorted by date.
 *
 * @returns {Array} - An array of post data objects.
 */
export const getSortedPostsData = () => {
    // Get file names under /posts
    const fileNames: string[] = fs.readdirSync(postsDirectory);

    const allPostsData: { id: string }[] = fileNames.map((fileName) => {
        // Remove ".md" from file name to get id
        const id: string = fileName.replace(/\.md$/, "");

        // Read markdown file as string
        const fullPath: string = path.join(postsDirectory, fileName);
        const fileContents: string = fs.readFileSync(fullPath, "utf8");

        // Use gray-matter to parse the post metadata section
        const matterResult: matter.GrayMatterFile<string> =
            matter(fileContents);

        // Combine the data with the id
        return {
            id,
            ...matterResult.data,
        };
    });

    // Sort posts by date
    return allPostsData.sort((a: PostData, b: PostData): number => {
        if (a.date < b.date) {
            return 1;
        } else {
            return -1;
        }
    });
};

/**
 * Retrieves an array of Post IDs from the postsDirectory.
 *
 * @returns {Array} - An array of Post IDs.
 */
export const getAllPostId = () => {
    const fileNames = fs.readdirSync(postsDirectory);

    return fileNames.map((fileName) => {
        return {
            params: {
                id: fileName.replace(/\.md$/, ""),
            },
        };
    });
};

/**
 * Retrieves the post data for a given ID
 * @param {string} id - The ID of the post
 * @returns {Promise<object>} The post data object
 */
export const getPostData = async (id: string) => {
    const fullPath = path.join(postsDirectory, `${id}.md`);
    const fileContents = fs.readFileSync(fullPath, "utf8");

    // Use gray-matter to parse the post metadata section
    const matterResult = matter(fileContents);

    // Use remark to convert markdown into HTML string
    const processedContent = await remark()
        .use(html)
        .process(matterResult.content);
    const contentHtml = processedContent.toString();

    return {
        id,
        contentHtml,
        ...(matterResult.data as { date: string; title: string }),
    };
};
