import styles from "../styles/main.module.scss";
import React from "react";

/**
 * Represents a Footer component.
 * @function Footer
 * @returns {JSX.Element} The Footer component.
 */
export const Footer: React.FC = (): React.JSX.Element => {
    return (
        <footer>
            <a
                href={
                    "https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                }
                target={"_blank"}
                rel={"noopener noreferrer"}>
                Powered by{" "}
                <img
                    src={"/vercel.svg"}
                    alt={"Vercel"}
                    className={styles.logo}
                />
            </a>
        </footer>
    );
};
