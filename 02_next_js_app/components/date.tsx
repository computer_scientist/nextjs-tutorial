import { parseISO, format } from "date-fns"; // npm install date-fns

/**
 * Represents a Date component.
 * @param {string} dateString - The date string in ISO format.
 * @returns {string|null} - The formatted date or null if the input is not a string.
 */
const Date = ({ dateString }: { dateString: string }) => {
    if (typeof dateString !== "string") {
        return null;
    }

    const date = parseISO(dateString);

    return <time dateTime={dateString}>{format(date, "LLLL d, yyyy")}</time>;
};
export default Date;
