import styles from "../styles/main.module.scss";
import Link from "next/link";
import React from "react";

/**
 * The Main component represents the main content of the page.
 *
 * @component
 * @returns {React.JSX.Element} The rendered main content.
 */
export const Main: React.FC = (): React.JSX.Element => {
    return (
        <main>
            <h1 className={styles.title}>
                Welcome to <Link href={"https://nextjs.org"}>Next.js</Link>
            </h1>
            <p className={styles.description}>
                Get started by editing <code>pages/index.js</code>
            </p>
            <div className={styles.grid}>
                <a href={"https://nextjs.org/docs"} className={styles.card}>
                    <h3>Documentation &rarr;</h3>
                    <p>
                        Find in-depth information about Next.js features and
                        API.
                    </p>
                </a>
                <a href={"https://nextjs.org/learn"} className={styles.card}>
                    <h3>Learn &rarr;</h3>
                    <p>
                        Learn about Next.js in an interactive course with
                        quizzes!
                    </p>
                </a>
                <a
                    href={
                        "https://github.com/vercel/next.js/tree/master/examples"
                    }
                    className={styles.card}>
                    <h3>Examples &rarr;</h3>
                    <p>
                        Discovery and deploy boilerplate example Next.js
                        projects.
                    </p>
                </a>
                <a
                    href={
                        "https://vercel.com/import?filter=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                    }
                    className={styles.card}>
                    <h3>Deploy &rarr;</h3>
                    <p>
                        Instantly deploy your Next.js site to a public URL with
                        Vercel.
                    </p>
                </a>
                <Link href={"blog"} className={styles.card}>
                    <h3>Blog &rarr;</h3>
                    <p>
                        Checkout the blog page to view a list of interesting
                        topics.
                    </p>
                </Link>
            </div>
        </main>
    );
};
