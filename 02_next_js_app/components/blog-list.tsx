import styles from "../styles/main.module.scss";
import Link from "next/link";
import Date from "./date";
import React from "react";

/**
 * Renders a list of blog posts with their titles, dates, and links to their individual pages.
 *
 * @param {Object} allPostsData - An array of blog post data objects.
 * @param {string} allPostsData.date - The date of the blog post.
 * @param {string} allPostsData.title - The title of the blog post.
 * @param {string} allPostsData.id - The unique identifier of the blog post.
 *
 * @returns {JSX.Element} The rendered list of blog posts.
 */
export const BlogList = ({
    allPostsData,
}: {
    allPostsData: {
        date: string;
        title: string;
        id: string;
    }[];
}): React.JSX.Element => {
    return (
        <section className={`${styles.headingMd} ${styles.padding1px}`}>
            <ul className={styles.list}>
                {allPostsData.map(({ id, date, title }) => (
                    <Link href={`/posts/${id}`}>
                        <li
                            className={`${styles.listItem} ${styles.card}`}
                            key={id}>
                            <p>{title}</p>
                            <br />
                            <small className={styles.lightText}>
                                <Date dateString={date} />
                            </small>
                        </li>
                    </Link>
                ))}
            </ul>
        </section>
    );
};
