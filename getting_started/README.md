# Getting Started

This will explain how to set up a React project to use Next.js. This was taken from the
next.js official website's tutorial. The choice to choose the manual method was made due to
the use of JetBrains WebStorm IDE for web development which provides incredible integration with 
system level packages.

## Installation (Manual "non-fluff" method)

For a fresh project, initialize project for npm usage using the following command.

```bash
npm init
```

You need to have nodejs installed to access the npm ecosystem. Enter the following within
the root directory of the project using command line. This should then create package.json, 
package-lock.json, and node_modules directory.

```bash
npm install react@latest react-dom@latest next@latest
```

At the time of writing, here are the dependencies that is automatically entered in the 
package.json file.

```json
{
  "dependencies": {
    "next": "^13.4.12",
    "react": "^18.2.0",
    "react-dom": "^18.2.0"
  }
}
```

Add script for development. "dev" is for development, "build" is for production build, "start" is for
starting a production build. "lint" is to start lint integration features.

```json
{
  "scripts": {
    "dev": "next dev",
    "build": "next build",
    "start": "next start",
    "lint": "next lint"
  },
  "dependencies": {
    "next": "^13.4.12",
    "react": "^18.2.0",
    "react-dom": "^18.2.0"
  }
}
```

Move or create new index.js file in a pages directory

```bash
├── node_modules
    └── ...
├── README.md
├── package-lock.json
├── package.json
└── pages
    └── index.js
```

Run development server using the following command. (Defaults to http://localhost:3000)

```bash
npm run dev
```