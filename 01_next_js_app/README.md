# Next.js Official Tutorial App

This is from next.js official site's tutorial create a next.js app.

The approaches taken in this project slightly differ from the official site's version.

### Components

The components in the `index.js` file were separated and modularized compared to the original.
This made the Home function look very thin and readable. This modular approach ensures and
demonstrates that these components can be reused.

### Styles

Sass was used instead of jsx and css used in the original. Since Next.js has built in support
for sass, this project experimented with how to implement Sass. There was a mini 7-1 approach
used with slight compromise.

Since the raw elements cannot be regularly included in the styles, it was then included in the
globals.scss file. This includes all base styling and resets.

In order to use the globals.scss, the creation of the _app.js file was necessary. This is usually
a special file in Next.js applications it initializes all pages. This file also plays an important
role in server side rendering (ssr).